## Kubernetes

## Namespace

kubectl create namespace dawid
kubectl get ns

kubectl config set-context --current --namespace=dawid2

### Pod
kubectl get pods
kubectl logs pod 
kubectl describe pod
kubectl attach my-pod -i                            # Attach to Running Container
kubectl port-forward my-pod 5000:6000               # Listen on port 5000 on the local machine and forward to port 6000 on my-pod
kubectl exec my-pod -- ls /                         # Run command in existing pod (1 container case)
kubectl exec my-pod -c my-container -- ls /         # Run command in existing pod (multi-container case)
kubectl top pod POD_NAME --containers               # Show metrics for a given pod and its containers

### ReplicaSet
kubectl apply -f name.yaml
kubectl get pods
kubectl get pod --show-labels
kubectl get pods -o wide
kubectl delete pod ...
kubectl delete pod -l app=nginx
kubectl delete pod --all 
kubectl get pods
kubectl logs -f nginx-rs-dnkmq
kubectl get svc nginx-lb -o yaml

kubectl get rs -n grzegorz
kubectl delete rs (name-rs)

### Secrets

kubectl create secret docker-registry azurereg --docker-server=l...... --docker-username=labk8s --docker-password=..... --docker-email=test@gmail.com

### ConfigMap

kubectl create configmap httpd-conf --from-file=Kubernetes/Objects/05-SecretsConfigMaps/httpd.conf

### Deployment
kubectl scale --replicas=3 deployment/apache
kubectl autoscale deployment/apache --min=2 --max=6 --cpu-percent=80
kubectl set image deploy/contosouniversity contosouniversity=csharkworkshop.azurecr.io/contosouniversity:v1.1  --record=true
kubectl set resources deploy/contosouniversity -c=apache --limits=cpu=200m,memory=512Mi
kubectl rollout history deploy/contosouniversity
kubectl rollout status deploy/contosouniversity
kubectl rollout undo deploy/contosouniversity
kubectl rollout undo deployment/deploy/contosouniversity --to-revision=2 



