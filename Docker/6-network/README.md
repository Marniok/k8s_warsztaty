## Docker network

docker network create -d bridge network-1
docker run -d --network network-1 --name mynginx1 -p 8888:80 nginx
docker run -d --network network-1 --name mynginx2 -p 8889:80 nginx


docker network create -d bridge elk
docker run -d --name elasticsearch --net elk -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" elasticsearch:7.14.2
docker run -d --name kibana --net elk -e "ELASTICSEARCH_HOSTS=http://elasticsearch:9200" -p 5601:5601 kibana:7.14.2


docker run --name zabbix --network zabbix -e DB_SERVER_HOST="postgres" -e POSTGRES_USER="user" -e POSTGRES_PASSWORD="password1" -d zabbix/zabbix-server-pgsql

docker run --name postgres --network zabbix -d -e POSTGRES_PASSWORD=test123 postgres

docker network create zabbix