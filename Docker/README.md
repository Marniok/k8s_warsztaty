## Podstawowe Komendy:
docker ps
docker images
docker pull

docker run
docker run --name test -d -p 8080:80 -v /Users/dawidmarniok/Elements:/usr/share/nginx/html:ro nginx

docker start
docker stop
docker rm

docker logs

docker attach
docker exec -it 

docker cp 

docker commit

## Tworzenie i zarządzanie obrazami
docker images
docker save
docker load

docker build
docker build -t name-image -f dockerfile .
