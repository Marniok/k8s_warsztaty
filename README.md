## Szkolenie Docker, Docker-compose, Kubernetes

## Agenda

1. Docker
2. Docker-compose
3. Kubernetes

### Instalacje K8S
- microk8s https://ubuntu.com/tutorials/install-a-local-kubernetes-with-microk8s?&_ga=2.130288463.1183207898.1638980700-1174156906.1638871932#1-overview

### Środowisko pracy

- Docker, Docker-compose - własne laptopy lub serwer linux na Azure
- Kubernetes - klaster AKS azure

### Narzędzia

- docker
- kubectl
- VS Code

## Agenda 3 dzień

1. Podsumowanie 1 dnia z K8s
2. Teoria PersistenVolume, PersistentVolumeClaim, StatefullSet, DeamonSet, Job, CronJob
3. Ćwiczenie: postawić aplikację eshop składającą się z FE i BE (deployment + service load balancer):
- Utworzyć secret dla Azure Registry w celu pobrania obrazów
- Odpalić BE - obraz: labk8s.azurecr.io/eshoppublicapi:1.1, ENV - ASPNETCORE_ENVIRONMENT="Docker", baseUrls__apiBase="http://{nazwaserwisu-dlaBE}", baseUrls__webBase="http://{nazwaserwisu-dlaFE}", IMAGEPULLSECRET - azurereg, 
- Odpalić FE - obraz: labk8s.azurecr.io/eshopwebapp:1.1, ENV - ASPNETCORE_ENVIRONMENT="Docker", baseUrls__apiBase="http://{nazwaserwisu-dlaBE}", baseUrls__webBase="http://{nazwaserwisu-dlaFE}", IMAGEPULLSECRET - azurereg, 
- Utworzyć dwa serwisy typu LoadBalancer dla FE i BE
- Aplikacja powinna być dostępna pod IP zewnętrzym serwisu FE
4. Cwiczenia 2 - elk
a. postawic elk + kibana - deploymnets:
elk - 1 replica - image: elasticsearch:7.14.2, data: /var/lib/elasticsearch/data, ENV discovery.type=single-node", port 9200,
kibana - 1 replica - image: kibana:7.14.2, ENV: ELASTICSEARCH_URL=http://elasticsearch:9200, port 5601
- Utworzyć PVC i podłączyć pod data ELK - e.g. k8s_warsztaty/Kubernetes/Objects/06-PVPVC/pvc-azure.yaml
b. postgres
- postawic postgres - k8s_warsztaty/Kubernetes/Objects/04-Deployments/02-deployment-postgres.yaml, data location: /var/lib/postgresql/data/pgdata
- Utworzyć PVC i podłączyć pod data postgres 

Zawsze tworzymy service per deployment - LoadBalancer 
W przypadku elk mozemy wystawic service - ClusterIP

5. Annotanions, Affinity, 
6. Helm Chart - https://artifacthub.io/
7. Monitoring i Alertowanie